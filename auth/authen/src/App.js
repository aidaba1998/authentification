import React from 'react';
import  './App';
import{useState} from "react";
import { createUserWithEmailAndPassword,onAuthStateChanged,signOut,signInWithEmailAndPassword } from 'firebase/auth';
import{auth} from './firebase';
import 'bootstrap/dist/css/bootstrap.min.css';
function App() {
 
  const[registerEmail,setRegisterEmail]=useState(""); 
  const[registerPassword,setRegisterPassword ]=useState(""); 
  const[loginEmail,setLoginEmail]=useState(""); 
  const[loginPassword,setLoginPassword]=useState("");
  const [user,setUser]=useState({}); 
  onAuthStateChanged(auth,(currentUser)=>{
    console.log(currentUser);
    setUser(currentUser);
  })
  const register=async()=>{
    try{
      const user= await createUserWithEmailAndPassword(auth,registerEmail,registerPassword);
      console.log(user); 
    }catch (error){
      console.log(error.message);
    }
  
  };   
  const login=async()=>{
    try{
      const user= await signInWithEmailAndPassword(auth,registerEmail,registerPassword);
      console.log(user); 
    }catch (error){
      console.log(error.message);
    }
  
  };  
  const logout =async()=>{
    await signOut(auth);
  };
  return (
    <div className='text-center'>
    <div className='Container text-center'>
  <div className='row'>
    <div className='col-md-6 ' >
      <form  style={{border:'1px solid',height:'120%',width:'60%',marginTop:'5%',marginLeft:'10%',boxShadow:'2px 3px 2px',borderRadius:'5%'}} >
      <div className='col-md-8' >
        <p className="h4 text-center mb-4">Rgister User</p>
       
        <input type="email" className="form-control"  onChange={(e)=>{setRegisterEmail(e.target.value)}}/>
        <br />
        <input type="password"  className="form-control" onChange={(e)=>{setRegisterPassword(e.target.value)}} />
        <div className="text-center mt-5">
          <button className='btn btn-info ' onClick={register}>Create User</button>
        </div>
        </div>
       </form>
    </div>
    <div className='col-md-6' >
      <form style={{border:'1px solid',height:'120%',width:'60%',boxShadow:'2px 3px 2px',borderRadius:'5%' ,marginTop:'5%'}}>
        <div className='col-md-8 pd-4' >
        <p className="h4 text-center mb-4">Login</p>
        
        <input type="email"  className="form-control"  onChange={(e)=>{setLoginEmail(e.target.value)}}/>
        <br />
        <input type="password"  className="form-control" onChange={(e)=>{setLoginPassword(e.target.value)}} />
        <div className="text-center mt-5">
          <button className='btn btn-info' onClick={login}>Login</button>
        </div>
       </div>
      </form>
    </div>
  </div>
</div>
    <div style={{marginTop:'10%'}}>
    <h4 >User Logged In:</h4>
    {user?.email} 
    <button  className='btn btn-danger m-4' onClick={logout}>Sign Out</button>
      
    </div>
    
    </div>
  )
}

export default App
