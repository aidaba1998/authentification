
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyBBbV2EOxsWSnTZRapFgg6F0i6g3WIOPXs",
  authDomain: "authentification-15fff.firebaseapp.com",
  projectId: "authentification-15fff",
  storageBucket: "authentification-15fff.appspot.com",
  messagingSenderId: "136022389842",
  appId: "1:136022389842:web:45e228b24764cc0d469b8b",
  measurementId: "G-7Q8RYMX70M"
};


const app = initializeApp(firebaseConfig);
export const auth= getAuth(app);